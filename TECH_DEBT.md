#TODO
* Reduce navbar code duplication by universal directive [MINOR]
* Reduce backend requests amount [MINOR]
* Add pagination or lazy-grid [MAJOR]
* Rework entity ids getters [MAJOR]
* Improve auto-complete algorithm [MINOR]
* Reduce Task status code duplication [MAJOR]
* Rework task filtration [MAJOR]
* Add ES6 support and simplify code with it [MAJOR]

