(function () {
    'use strict';

    angular
        .module('taskTrackerApp')
        .constant('environment', {
            api: 'http://localhost:8080/'
        })
        .constant('TaskStatus', {
            WAITING: 'WAITING',
            IMPLEMENTATION: 'IMPLEMENTATION',
            VERIFYING: 'VERIFYING',
            RELEASING: 'RELEASING'
        })
        .constant('Role', {
            MANAGER: 'MANAGER',
            DEVELOPER: 'DEVELOPER'
        });
})();
