(function () {

    'use strict';

    angular
        .module('taskTrackerApp')
        .service('ProjectApi', ProjectApi);

    /**
     * @ngInject
     */
    function ProjectApi($http, environment) {

        var self = this;
        var api = environment.api;

        self.getProject = function (projectId) {
            return $http.get(api + 'projects/' + projectId);
        };

        self.getManager = function (projectId) {
            return $http.get(api + 'projects/' + projectId + '/manager');
        };

        self.getTasks = function (projectId, reporterId) {
            if (arguments.length === 2) {
                return $http.get(api + 'tasks/search/findAllByProjectIdAndReporterId?projectId=' + projectId + '&reporterId=' + reporterId);
            } else {
                return $http.get(api + 'projects/' + projectId + '/tasks');
            }
        };

        self.getTasksCount = function (projectId) {
            return $http.get(api + 'tasks/search/countByProjectId?id=' + projectId);
        };

        self.createTask = function (task) {
            return $http.post(api + 'tasks', task);
        };

        self.getDevelopers = function (projectId) {
            return $http.get(api + 'projects/' + projectId + '/developers');
        };

        self.addDeveloper = function (userId, projectId) {
            return $http.post(api + 'project/add-developer', {
                'userId': userId,
                'projectId': projectId
            });
        };

        self.searchUsersByPartOfName = function (name) {
            return $http.get(api + 'users/search/searchUsersByPartOfName?name=' + name + '&size=5');
        };

    }

})();
