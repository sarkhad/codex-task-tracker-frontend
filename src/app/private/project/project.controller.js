(function () {

    'use strict';

    angular
        .module('taskTrackerApp')
        .config(ProjectStateRouter)
        .controller('ProjectController', ProjectController);

    /**
     * @ngInject
     */
    function ProjectStateRouter($stateProvider) {
        $stateProvider.state('project', {
            url: '/project/{id}',
            templateUrl: 'app/private/project/project.html',
            controller: 'ProjectController',
            controllerAs: 'project'
        });
    }

    /**
     * @ngInject
     */
    function ProjectController($stateParams, $state, ProjectApi, $scope, $translate, AuthenticationService, $localStorage, TaskStatus) {

        var self = this;

        self.$onInit = function () {
            if (!$stateParams.id) {
                $state.go('dashboard');
                return;
            }

            self.input = '';
            self.suggestedDevelopersNames = [''];
            self.showAddDeveloperForm = false;

            ProjectApi.getProject($stateParams.id).then(function (response) {
                self = _.extend(self, response.data);
            }, function () {
                $state.go('dashboard');
            });
            ProjectApi.getManager($stateParams.id).then(function (response) {
                self.manager = response.data;
            }, function () {
            });
            ProjectApi.getTasksCount($stateParams.id).then(function (response) {
                self.tasksCount = response.data;
            }, function () {
            });

            self.getTasks();
            getDevelopers();


            self.authService = AuthenticationService;
        };

        self.getTaskBadgeColor = function (status) {
            switch (status) {
                case TaskStatus.WAITING:
                    return 'blue';
                case TaskStatus.IMPLEMENTATION:
                    return 'red';
                case TaskStatus.VERIFYING:
                    return 'yellow darken-3';
                case TaskStatus.RELEASING:
                    return 'green';
            }
        };

        self.getTasks = function () {
            if (self.showAllTasks) {
                ProjectApi.getTasks($stateParams.id, $localStorage.currentUser.id).then(function (response) {
                    self.tasks = ((response.data._embedded || {}).tasks || []);
                }, function () {
                    self.tasks = [];
                });
            } else {
                ProjectApi.getTasks($stateParams.id).then(function (response) {
                    self.tasks = ((response.data._embedded || {}).tasks || []);
                }, function () {
                    self.tasks = [];
                });
            }
        };

        function getDevelopers() {
            ProjectApi.getDevelopers($stateParams.id).then(function (response) {
                self.developers = response.data._embedded.users;
            }, function () {
                self.developers = [];
            });
        }

        self.getManagerFullName = function () {
            var firstName = (self.manager || {}).firstName;
            var lastName = (self.manager || {}).lastName;
            return firstName + ' ' + lastName;
        };

        /**
         * @param task {{object}}
         */
        self.getTaskId = function (task) {
            return _.last(task._links.self.href.split('/'));
        };

        self.addDeveloper = function () {
            var userId = self.input.split(':')[0];

            if (!angular.isNumber(parseInt(userId))) {
                return;
            }

            ProjectApi.addDeveloper(userId, $stateParams.id).then(function () {
                $translate('PROJECT.DEVELOPERS.ADDED_DEVELOPER').then(function (message) {
                    Materialize.toast(message, 3000);
                    self.input = '';
                    self.showAddDeveloperForm = false;
                    getDevelopers();
                });
            });
        };

        $scope.$watch('project.input', function (newVal) {
            ProjectApi.searchUsersByPartOfName(newVal).then(function (response) {
                self.suggestedDevelopers = ((response.data._embedded || {}).users || [])
                    .filter(function (user) {
                        return _.includes(user.roles, 'DEVELOPER');
                    }).filter(function (developer) {
                        return angular.isUndefined(_.find(self.developers, function (o) {
                            return o.id === developer.id;
                        }));
                    }).map(function (developer) {
                        return {
                            id: developer.id,
                            firstName: developer.firstName,
                            lastName: developer.lastName
                        };
                    });
                self.suggestedDevelopersNames = _.map(self.suggestedDevelopers, function (user) {
                    return (user.id + ': ' + user.firstName + ' ' + user.lastName);
                });
            });
        });

        self.getProjectId = function () {
            return $stateParams.id;
        };

    }


})();

