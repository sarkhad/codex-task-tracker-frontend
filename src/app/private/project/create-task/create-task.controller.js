(function () {

    'use strict';

    angular
        .module('taskTrackerApp')
        .config(ProjectCreateTaskStateRouter)
        .controller('ProjectCreateTaskController', ProjectCreateTaskController);

    /**
     * @ngInject
     */
    function ProjectCreateTaskStateRouter($stateProvider) {
        $stateProvider.state('project.createTask', {
            url: '/create-task',
            templateUrl: 'app/private/project/create-task/create-task.html',
            controller: 'ProjectCreateTaskController',
            controllerAs: 'creator',
            parent: 'project'
        });
    }

    /** @ngInject */
    function ProjectCreateTaskController(ProjectApi, $state, $translate, $localStorage, AuthenticationService, $stateParams) {

        var self = this;

        self.$onInit = function () {
            self.statuses = ['WAITING', 'IMPLEMENTATION', 'VERIFYING', 'RELEASING'];
            self.status = _.first(self.statuses);
            self.isCreating = false;
        };

        self.create = function () {
            self.isCreating = true;
            ProjectApi.createTask({
                'name': self.name,
                'description': self.description,
                'status': self.status,
                'assignee': 'users/' + $localStorage.currentUser.id,
                'project': 'projects/' + $stateParams.id
            }).then(function (response) {
                $translate('PROJECT.CREATE_TASK.TASK_CREATED').then(function (message) {
                    Materialize.toast(message, 2000, '', function () {
                        $state.go('task', {id: response.data.id});
                    });
                });
            });
        };

    }

})();
