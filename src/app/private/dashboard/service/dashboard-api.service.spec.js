(function () {

    'use strict';

    describe('Dashboard Api Service', function () {

        var httpBackend, service, api;

        beforeEach(function () {
            angular.mock.module('taskTrackerApp');
            angular.mock.inject(function (DashboardApi, $httpBackend, environment) {
                service = DashboardApi;
                httpBackend = $httpBackend;
                api = environment.api;
            });
        });

        describe('Create Project Request', function () {
            it('should send post request with project', function () {
                var projectName = 'Task Tracker';
                var projectDescription = 'Application for projects managing';
                var projectManagerId = 14;

                var project = {
                    name: projectName,
                    description: projectDescription,
                    manager: 'users/' + projectManagerId
                };

                service.createProject(projectName, projectDescription, projectManagerId);

                httpBackend.expectPOST(api + 'projects', project).respond({});
                httpBackend.flush();
            });
        });

    });

})();
