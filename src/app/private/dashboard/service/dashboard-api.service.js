(function () {

    'use strict';

    angular
        .module('taskTrackerApp')
        .service('DashboardApi', DashboardApi);

    /**
     * @ngInject
     */
    function DashboardApi($http, environment) {

        var self = this;
        var api = environment.api;

        /**
         * Fetch projects developer participate in
         * @param developerId
         * @return {Promise}
         */
        self.getProjects = function (developerId) {
            return $http.get(api + 'users/' + developerId + '/projects');
        };

        self.createProject = function (name, description, managerId) {
            return $http.post(api + 'projects', {
                name: name,
                description: description,
                manager: 'users/' + managerId
            });
        };

        self.getSupervisedProjects = function (managerId) {
            return $http.get(api + 'projects/search/queryAllByManagerId?id=' + managerId);
        };

    }

})();
