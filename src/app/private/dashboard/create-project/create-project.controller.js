(function () {

    'use strict';

    angular
        .module('taskTrackerApp')
        .config(DashboardCreateProjectStateRouter)
        .controller('DashboardCreateProjectController', DashboardCreateProjectController);

    /**
     * @ngInject
     */
    function DashboardCreateProjectStateRouter($stateProvider) {
        $stateProvider.state('dashboard.createProject', {
            url: '/create-project',
            templateUrl: 'app/private/dashboard/create-project/create-project.html',
            controller: 'DashboardCreateProjectController',
            controllerAs: 'creator',
            parent: 'dashboard'
        });
    }

    /** @ngInject */
    function DashboardCreateProjectController(DashboardApi, $state, $translate, $localStorage, AuthenticationService) {

        var self = this;

        self.$onInit = function () {
            if (!AuthenticationService.hasRole('MANAGER')) {
                $state.go('dashboard');
            }
            self.isCreating = false;
        };

        self.create = function () {
            self.isCreating = true;
            DashboardApi.createProject(self.name, self.description, $localStorage.currentUser.id).then(function (response) {
                $translate('DASHBOARD.CREATE_PROJECT.PROJECT_CREATED').then(function (message) {
                    Materialize.toast(message, 2000, '', function () {
                        var projectId = _.last(response.data._links.self.href.split('/'));
                        $state.go('project', {id: projectId});
                    });
                });
            });
        };

    }

})();
