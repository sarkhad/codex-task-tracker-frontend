(function () {

    'use strict';

    angular
        .module('taskTrackerApp')
        .config(DashboardStateRouter)
        .controller('DashboardController', DashboardController);

    /** @ngInject */
    function DashboardStateRouter($stateProvider) {
        $stateProvider.state('dashboard', {
            url: '/dashboard',
            templateUrl: 'app/private/dashboard/dashboard.html',
            controller: 'DashboardController',
            controllerAs: 'dashboard'
        });
    }

    /**
     * @ngInject
     */
    function DashboardController(DashboardApi, $localStorage, AuthenticationService) {

        var self = this;

        self.$onInit = function () {
            if (self.isManager()) {
                DashboardApi.getSupervisedProjects($localStorage.currentUser.id).then(function (response) {
                    self.supervisedProjects = ((response.data._embedded || {}).projects || []);
                    self.supervisedProjectsPage = _.extend(self.supervisedProjectsPage, response.data.page);
                }, function () {
                    self.supervisedProjects = [];
                    self.supervisedProjectsPage = {};
                });
            } else if (self.isDeveloper()) {
                DashboardApi.getProjects($localStorage.currentUser.id).then(function (response) {
                    self.projects = ((response.data._embedded || {}).projects || []);
                }, function () {
                    self.projects = [];
                });
            }
        };

        /**
         * @param project {{object}}
         */
        self.getProjectId = function (project) {
            return _.last((((project._links || {}).self || {}).href || '').split('/'));
        };

        self.isManager = function () {
            return AuthenticationService.hasRole('MANAGER');
        };

        self.isDeveloper = function () {
            return AuthenticationService.hasRole('DEVELOPER');
        };

    }


})();
