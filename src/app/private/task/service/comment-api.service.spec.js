(function () {

    'use strict';

    describe('Comment Api Service', function () {

        var service, httpBackend, api;

        beforeEach(function () {
            angular.mock.module('taskTrackerApp');
            angular.mock.inject(function (CommentApi, $httpBackend, environment) {
                service = CommentApi;
                api = environment.api;
                httpBackend = $httpBackend;
            });
        });

        afterEach(function () {
            httpBackend.verifyNoOutstandingExpectation();
            httpBackend.verifyNoOutstandingRequest();
        });

        describe('Delete Comment Request', function () {
            it('should send delete request with given comment id', function () {
                var commentId = 2;

                service.deleteComment(commentId);

                httpBackend.expectDELETE(api + 'comments/2').respond({});
                httpBackend.flush();
            });
        });

        describe('Change Comment Content Request', function () {
            it('should send patch request with given comment id and comment content', function () {
                var commentId = 12;
                var content = 'comment content';

                service.changeCommentContent(commentId, content);

                httpBackend.expectPATCH(api + 'comments/12', {content: content}).respond({});
                httpBackend.flush();
            });
        });

        describe('Get Comment Author Request', function () {
            it('should send get request with given comment id', function () {
                var commentId = 22;

                service.getCommentAuthor(commentId);

                httpBackend.expectGET(api + 'comments/22/author').respond({});
                httpBackend.flush();
            });
        });

        describe('Get Comment Request', function () {
            it('should send get request with given comment id', function () {
                var commentId = 3;

                service.getComment(commentId);

                httpBackend.expectGET(api + 'comments/3').respond({});
                httpBackend.flush();
            });
        });

    });

})();
