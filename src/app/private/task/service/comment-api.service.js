(function () {

    'use strict';

    angular
        .module('taskTrackerApp')
        .service('CommentApi', CommentApi);

    /**
     * @ngInject
     */
    function CommentApi($http, environment) {

        var self = this;
        var api = environment.api;

        self.deleteComment = function (commentId) {
            return $http.delete(api + 'comments/' + commentId);
        };

        self.changeCommentContent = function (commentId, content) {
            return $http.patch(api + 'comments/' + commentId, {content: content});
        };

        self.getCommentAuthor = function (commentId) {
            return $http.get(api + 'comments/' + commentId + '/author');
        };

        self.getComment = function (commentId) {
            return $http.get(api + 'comments/' + commentId);
        };

    }


})();
