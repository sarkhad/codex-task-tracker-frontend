(function () {

    'use strict';

    angular
        .module('taskTrackerApp')
        .service('TaskApi', TaskApi);

    /**
     * @ngInject
     */
    function TaskApi($http, environment) {

        var self = this;
        var api = environment.api;

        self.getTask = function (taskId) {
            return $http.get(api + 'tasks/' + taskId);
        };

        self.getReporter = function (taskId) {
            return $http.get(api + 'tasks/' + taskId + '/reporter');
        };

        self.getAssignee = function (taskId) {
            return $http.get(api + 'tasks/' + taskId + '/assignee');
        };

        self.setTaskStatus = function (taskId, status) {
            return $http.patch(api + 'tasks/' + taskId, {status: status});
        };

        self.setReporter = function (taskId, reporterId) {
            return $http.patch(api + 'tasks/' + taskId, {
                reporter: 'users/' + reporterId
            });
        };

        self.getComments = function (taskId) {
            return $http.get(api + 'tasks/' + taskId + '/comments');
        };

        self.addComment = function (content, taskId, userId) {
            return $http.post(api + 'comments', {
                content: content,
                task: 'tasks/' + taskId,
                author: 'users/' + userId
            });
        };

        self.getProject = function (taskId) {
            return $http.get(api + 'tasks/' + taskId + '/project');
        };

        self.getCommentIds = function (taskId) {
            return $http.get(api + 'tasks/' + taskId + '/comments');
        };

    }


})();
