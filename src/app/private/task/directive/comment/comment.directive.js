(function () {

    'use strict';

    angular
        .module('taskTrackerApp')
        .directive('comment', CommentDirective)
        .controller('CommentController', CommentController);

    /**
     * @ngInject
     */
    function CommentDirective() {
        return {
            restrict: 'E',
            templateUrl: 'app/private/task/directive/comment/comment.html',
            controller: 'CommentController',
            controllerAs: 'comment',
            scope: {
                id: '@commentId'
            }
        };
    }

    /**
     * @ngInject
     */
    function CommentController(CommentApi, $translate, $scope) {

        var self = this;

        self.$onInit = function () {
            getComment();

            self.inEditing = false;
        };

        function getComment() {
            CommentApi.getComment($scope.id).then(function (response) {
                self = _.extend(self, response.data);
            });
            CommentApi.getCommentAuthor($scope.id).then(function (response) {
                self.author = _.extend({}, response.data);
            });
        }

        self.getAuthorFullName = function () {
            var firstName = self.firstName;
            var lastName = self.lastName;
            return firstName + ' ' + lastName;
        };

        self.edit = function () {
            self.inEditing = true;
            self.newContent = self.content;
        };

        self.delete = function () {
            CommentApi.deleteComment(self.id).then(function () {
                $translate('TASK.COMMENTS.DELETED').then(function (message) {
                    Materialize.toast(message, 3000);
                    $scope.$emit('taskCommentDeleted', self.id);
                });
            });
        };

        self.save = function () {
            CommentApi.changeCommentContent(self.id, self.newContent).then(function () {
                $translate('TASK.COMMENTS.EDITED').then(function (message) {
                    Materialize.toast(message, 3000);
                    self.inEditing = false;
                    $scope.$emit('taskCommentEdited');
                });
            });
        };

        self.getAuthorFullName = function () {
            var firstName = (self.author || {}).firstName;
            var lastName = (self.author || {}).lastName;
            if (firstName && lastName) {
                return firstName + ' ' + lastName;
            } else {
                return '';
            }
        }
        ;

    }

})
();
