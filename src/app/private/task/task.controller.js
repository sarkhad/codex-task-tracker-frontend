(function () {

    'use strict';

    angular
        .module('taskTrackerApp')
        .config(TaskStateRouter)
        .controller('TaskController', TaskController);

    /**
     * @ngInject
     */
    function TaskStateRouter($stateProvider) {
        $stateProvider.state('task', {
            url: '/task/{id}',
            templateUrl: 'app/private/task/task.html',
            controller: 'TaskController',
            controllerAs: 'task'
        });
    }

    /**
     * @ngInject
     */
    function TaskController($stateParams, $state, TaskApi, ProjectApi, $scope, $translate, $localStorage, AuthenticationService) {

        var self = this;

        self.$onInit = function () {
            if (!$stateParams.id) {
                $state.go('dashboard');
                return;
            }

            TaskApi.getTask($stateParams.id).then(function (response) {
                self = _.extend(self, response.data);
            }, function () {
                $state.go('dashboard');
            });
            TaskApi.getAssignee($stateParams.id).then(function (response) {
                self.assignee = response.data;
            }, function () {
            });
            TaskApi.getProject($stateParams.id).then(function (response) {
                self.project = response.data;
            }).then(function () {
                ProjectApi.getDevelopers(self.project.id).then(function (response) {
                    self.project.developers = ((response.data._embedded || {}).users || []);
                });
            });

            getComments();
            getReporter();

            self.init = true;
            self.statuses = ['WAITING', 'IMPLEMENTATION', 'VERIFYING', 'RELEASING'];

            $scope.$watch('task.status', function (newValue, oldValue) {
                if (angular.isUndefined(newValue) || angular.isUndefined(oldValue)) {
                    return;
                }
                TaskApi.setTaskStatus($stateParams.id, newValue).then(function () {
                    $translate('TASK.STATUS_UPDATED').then(function (message) {
                        Materialize.toast(message, 3000);
                    });
                });
            });

            $scope.$watch('task.newReporter', function (newValue, oldValue) {
                if (angular.isUndefined(newValue)) {
                    return;
                }
                if (newValue === (self.reporter || {}).id) {
                    return;
                }
                if (newValue == oldValue) {
                    return;
                }

                TaskApi.setReporter($stateParams.id, newValue).then(function () {
                    $translate('TASK.DEVELOPER_ASSIGNED').then(function (message) {
                        Materialize.toast(message, 3000);
                        getReporter();
                    });
                });
            });

            //Just reloading all comments. Don't care about optimization
            $scope.$on('taskCommentDeleted', function () {
                getComments();
            });
            $scope.$on('taskCommentEdited', function () {
                getComments();
            });

            self.authService = AuthenticationService;
        };

        function getComments() {
            TaskApi.getComments($stateParams.id).then(function (response) {
                self.comments = response.data._embedded.comments;
            });
        }

        function getReporter() {
            TaskApi.getReporter($stateParams.id).then(function (response) {
                self.reporter = response.data;
                self.newReporter = self.reporter.id;
            }, function () {
            });
        }

        self.addComment = function () {
            TaskApi.addComment(self.newComment, $stateParams.id, $localStorage.currentUser.id).then(function () {
                $translate('TASK.ADD_COMMENT.POSTED').then(function (message) {
                    Materialize.toast(message, 3000);
                    getComments();
                    self.newComment = '';
                });
            });
        };

        self.getAssigneeFullName = function () {
            var firstName = ((self || {}).assignee || {}).firstName;
            var lastName = ((self || {}).assignee || {}).lastName;
            return firstName + ' ' + lastName;
        };

        self.getReporterFullName = function () {
            var firstName = ((self || {}).reporter || {}).firstName;
            var lastName = ((self || {}).reporter || {}).lastName;
            if (firstName && lastName) {
                return firstName + ' ' + lastName;
            } else {
                return 'Not assigned';
            }
        };

    }

})();
