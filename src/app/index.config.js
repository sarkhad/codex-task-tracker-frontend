(function() {
    'use strict';

    angular
        .module('taskTrackerApp')
        .config(config);

    /** @ngInject */
    function config($logProvider, $locationProvider, $translateProvider) {
        // Enable log
        $logProvider.debugEnabled(true);

        $locationProvider.html5Mode(true);

        $translateProvider.useStaticFilesLoader({
            prefix: 'assets/i18n/',
            suffix: '.json'
        });
        $translateProvider.useSanitizeValueStrategy('sanitize');
        $translateProvider.preferredLanguage('en');
    }

})();
