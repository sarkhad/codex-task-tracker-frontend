(function () {

    'use strict';

    angular
        .module('taskTrackerApp')
        .config(SigninStateRouter)
        .controller('SigninController', SigninController);

    /**
     * @ngInject
     */
    function SigninStateRouter($stateProvider) {
        $stateProvider.state('signin', {
            url: '/signin?token',
            templateUrl: 'app/public/signin/signin.html',
            controller: 'SigninController',
            controllerAs: 'signin'
        });
    }

    /**
     * @ngInject
     */
    function SigninController(AuthenticationService, $state, SigninApi, SignupApi, $q, $translate, $stateParams) {

        var self = this;

        self.$onInit = function () {
            AuthenticationService.logout();

            if ($stateParams.token) {
                SignupApi.confirm($stateParams.token).then(function () {
                    $translate('SIGNIN.ACCOUNT_ENABLED').then(function (message) {
                        Materialize.toast(message, 5000);
                        $state.transitionTo('signin');
                    });
                }, function () {
                });
            }
        };

        self.login = function () {
            AuthenticationService.authenticate(self.user.username, self.user.password, function (result) {
                if (result === true) {
                    $state.go('dashboard');
                } else {
                    $translate('SIGNIN.USER_NOT_EXIST_OR_DISABLED').then(function (message) {
                        Materialize.toast(message, 4000);
                    });
                }
            });
        };

        self.isValidEmail = function (email) {
            var deferred = $q.defer();

            SigninApi.isValidEmail(email).then(function (response) {
                deferred.resolve({isValid: response.data.isValid, message: response.data.message});
            }, function () {
                deferred.resolve({isValid: false});
            });

            return deferred.promise;
        };

    }

})();
