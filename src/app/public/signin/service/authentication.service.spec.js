(function () {

    'use strict';

    describe('Authentication Service', function () {

        var httpBackend, service, localStorage, http;

        beforeEach(function () {
            angular.mock.module('taskTrackerApp');
            angular.mock.inject(function (AuthenticationService, $httpBackend, $localStorage, $http) {
                service = AuthenticationService;
                httpBackend = $httpBackend;
                localStorage = $localStorage;
                http = $http;
            });
        });

        afterEach(function () {
            httpBackend.verifyNoOutstandingExpectation();
            httpBackend.verifyNoOutstandingRequest();
        });

        describe('Has Role Predicate', function () {
            it('should be true if user have given role', function () {
                localStorage.currentUser = {
                    roles: [
                        'MANAGER',
                        'DEVELOPER'
                    ]
                };

                expect(service.hasRole('MANAGER')).toBe(true);
            });
            it('should be false if user have not given role', function () {
                localStorage.currentUser = {
                    roles: [
                        'MANAGER'
                    ]
                };

                expect(service.hasRole('DEVELOPER')).toBe(false);
            });
        });

        describe('Has Role Predicate', function () {
            it('should be true if local storage contain user', function () {
                localStorage.currentUser = {
                    id: 1,
                    firstName: 'John',
                    lastName: 'Smith',
                    username: 'j.smith@example.com'
                };

                expect(service.isLoggedIn()).toBe(true);
            });
            it('should be false if local storage do not contain user', function () {
                delete localStorage.currentUser;

                expect(service.isLoggedIn()).toBe(false);
            });
        });

        describe('Log Out Function', function () {
            it('should delete user from local storage', function () {
                localStorage.currentUser = {
                    username: 'j.smith@example.com',
                    roles: [
                        'MANAGER'
                    ]
                };

                service.logout();

                expect(localStorage.currentUser).not.toBeDefined();
            });
        });

    });

})();
