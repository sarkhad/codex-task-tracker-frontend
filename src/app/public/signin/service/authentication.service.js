﻿(function () {
    'use strict';

    angular
        .module('taskTrackerApp')
        .service('AuthenticationService', AuthenticationService);

    /**
     * @ngInject
     */
    function AuthenticationService(SigninApi, $localStorage, $http) {

        var self = this;

        /**
         * Authenticate user on the backend
         * @param username {String}
         * @param password {String}
         * @param callback {Function}
         */
        self.authenticate = function (username, password, callback) {
            SigninApi.signin(username, password).then(function (response) {
                if (response.data.token) {
                    if (response.data.token !== 'disabled') {
                        $localStorage.currentUser = {token: response.data.token};

                        $http.defaults.headers.common.Authorization = 'Bearer ' + response.data.token;

                        SigninApi.getCurrentUser().then(function (response) {
                            $localStorage.currentUser = _.extend($localStorage.currentUser, response.data);
                            callback(true);
                        });
                    } else {
                        callback(false);
                    }
                } else {
                    callback(false);
                }
            });
        };

        /**
         * @return {boolean} if user signed in
         */
        self.isLoggedIn = function () {
            return !!$localStorage.currentUser;
        };

        /**
         * @param role {String}
         * @return {boolean} if user has role
         */
        self.hasRole = function (role) {
            return _.includes((($localStorage.currentUser || {}).roles || {}), role.toUpperCase());
        };

        self.logout = function () {
            delete $localStorage.currentUser;
            $http.defaults.headers.common.Authorization = '';
        };

    }

})();
