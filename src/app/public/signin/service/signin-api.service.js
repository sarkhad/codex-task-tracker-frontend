(function () {

    'use strict';

    angular
        .module('taskTrackerApp')
        .service('SigninApi', SigninApi);

    /**
     * @ngInject
     */
    function SigninApi($http, environment) {

        var self = this;
        var api = environment.api;

        self.getCurrentUser = function () {
            return $http.get(api + 'current');
        };

        self.signin = function (username, password) {
            return $http.get(api + 'signin', {params: {username: username, password: password}});
        };

        self.isValidEmail = function (email) {
            return $http.get(api + 'signin/is-valid-email', {params: {email: email}});
        };

    }

})();
