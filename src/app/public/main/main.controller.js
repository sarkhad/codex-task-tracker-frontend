(function () {

    'use strict';

    angular
        .module('taskTrackerApp')
        .config(MainStateRouter)
        .controller('MainController', MainController);

    /**
     * @ngInject
     */
    function MainStateRouter($stateProvider) {
        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: 'app/public/main/main.html',
                controller: 'MainController',
                controllerAs: 'main'
            });
    }

    /**
     * @ngInject
     */
    function MainController(AuthenticationService) {

        var self = this;

        self.$onInit = function () {
            self.authService = AuthenticationService;
        };

    }

})();
