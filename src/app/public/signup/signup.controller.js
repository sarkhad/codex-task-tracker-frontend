(function () {

    'use strict';

    angular
        .module('taskTrackerApp')
        .config(SignupStateRouter)
        .controller('SignupController', SignupController);

    /**
     * @ngInject
     */
    function SignupStateRouter($stateProvider) {
        $stateProvider.state('signup', {
            url: '/signup',
            templateUrl: 'app/public/signup/signup.html',
            controller: 'SignupController',
            controllerAs: 'signup'
        });
    }

    /**
     * @ngInject
     */
    function SignupController($state, $translate, $q, SignupApi, AuthenticationService) {

        var self = this;

        self.$onInit = function () {
            AuthenticationService.logout();

            self.isRegistrating = false;
        };

        self.register = function () {
            self.isRegistrating = true;
            SignupApi.register(self.user).then(function (response) {
                if (response.status === 201) {
                    $translate('SIGNUP.CONFIRM_EMAIL').then(function (message) {
                        Materialize.toast(message, 5000, '', function () {
                            $state.go('signin');
                        });
                    });
                }
            }, function (response) {
                if (response.status === 302) {
                    $translate('SIGNUP.USER_ALREADY_EXIST').then(function (message) {
                        Materialize.toast(message, 4000);
                    });
                }
            });
        };

        self.isValidEmail = function (email) {
            var deferred = $q.defer();

            SignupApi.isValidEmail(email).then(function (response) {
                deferred.resolve({isValid: response.data.isValid, message: response.data.message});
            }, function () {
                deferred.resolve({isValid: false});
            });

            return deferred.promise;
        };

    }

})();
