(function () {

    'use strict';

    angular
        .module('taskTrackerApp')
        .service('SignupApi', SignupApi);

    /**
     * @ngInject
     */
    function SignupApi($http, environment) {

        var self = this;
        var api = environment.api;

        self.register = function (user) {
            return $http.post(api + 'signup', user, {});
        };

        self.isValidEmail = function (email) {
            return $http.get(api + 'signup/is-valid-email', {params: {email: email}});
        };

        self.confirm = function (token) {
            return $http.post(api + 'signup/confirm', token);
        };

    }


})();
