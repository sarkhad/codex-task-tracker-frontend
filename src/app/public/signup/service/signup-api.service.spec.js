(function () {

    'use strict';

    describe('Signup Api Service', function () {

        var httpBackend, service, api;

        beforeEach(function () {
            angular.mock.module('taskTrackerApp');
            angular.mock.inject(function (SignupApi, $httpBackend, environment) {
                service = SignupApi;
                httpBackend = $httpBackend;
                api = environment.api;
            });
        });

        describe('Register User Request', function () {
            it('should send post request with user', function () {
                var user = {
                    firstName: 'John',
                    lastName: 'Smith',
                    email: 'j.smith@example.com',
                    password: 'john_the_best',
                    isManager: true
                };

                service.register(user);

                httpBackend.expectPOST(api + 'signup', user).respond({});
                httpBackend.flush();
            });
        });

        describe('Validate Email Request', function () {
            it('should send get request with email', function () {
                var email = 'j.smith@example.com';

                service.isValidEmail(email);

                httpBackend.expectGET(api + 'signup/is-valid-email?email=' + email).respond({});
                httpBackend.flush();
            });
        });

        describe('Confirm Email Request', function () {
            it('should send post request with email', function () {
                var token = 'ai5zbWl0aEBleGFtcGxlLmNvbQ==';

                service.confirm(token);

                httpBackend.expectPOST(api + 'signup/confirm', token).respond({});
                httpBackend.flush();
            });
        });

    });

})();
