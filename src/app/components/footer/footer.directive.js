(function () {

    'use strict';

    angular
        .module('taskTrackerApp')
        .directive('footer', FooterDirective);

    /**
     * @ngInject
     */
    function FooterDirective() {
        return {
            restrict: 'E',
            templateUrl: 'app/components/footer/footer.html',
            controller: 'FooterController',
            controllerAs: 'footer',
            bindToController: true,
            transclude: true
        };
    }

})();
