(function () {

    'use strict';

    angular
        .module('taskTrackerApp')
        .controller('FooterController', FooterController);

    /**
     * @ngInject
     */
    function FooterController() {

        var self = this;

        self.$onInit = function () {

        };

    }

})();
