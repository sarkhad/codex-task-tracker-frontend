(function () {
    'use strict';

    var dependencies = [
        'ngCookies',
        'ngTouch',
        'ngSanitize',
        'ngStorage',
        'ngResource',
        'ui.router',
        'pascalprecht.translate',
        'ghiscoding.validation',
        'ui.materialize',
        'auto-complete'
    ];

    angular
        .module('taskTrackerApp', dependencies);

})();
