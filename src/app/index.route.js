(function () {

    'use strict';

    angular
        .module('taskTrackerApp')
        .config(routeConfig);

    /**
     * @ngInject
     */
    function routeConfig($urlRouterProvider) {
        $urlRouterProvider.otherwise('/');
    }

})();
