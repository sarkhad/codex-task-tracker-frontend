(function () {
    'use strict';

    angular
        .module('taskTrackerApp')
        .run(runBlock);

    /**
     * @ngInject
     */
    function runBlock($log, $rootScope, $http, $localStorage, $state, AuthenticationService) {
        if (AuthenticationService.isLoggedIn()) {
            $http.defaults.headers.common.Authorization = 'Bearer ' + $localStorage.currentUser.token;
        }

        $rootScope.$on('$stateChangeStart', function (event, toState) {
            var publicStates = ['signin', 'signup', 'home'];
            var restrictedState = !_.includes(publicStates, toState.name);

            if (restrictedState && !AuthenticationService.isLoggedIn()) {
                event.preventDefault();
                $state.go('signin');
            }

            $rootScope.loading = true;
        });

        $rootScope.$on('$stateChangeSuccess', function () {
            $rootScope.loading = false;
        });


        $log.debug('runBlock end');
    }

})();
